import { Component } from '@angular/core';
import { HTTP_PROVIDERS } from '@angular/http';

import { CustomersComponent } from './customers/customers.component';


@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html',
    directives: [CustomersComponent],
    providers: [HTTP_PROVIDERS]
})
export class AppComponent {

    title = "Customer App";
    name: string = "Ward"
    wardsColor = "green";

    // [] means property binding one way - up
    // () means event binding - dom to components

    changeSuitColor(): void {
        this.wardsColor = this.wardsColor ===  'green' ? 'red' : 'green';
    }

}
